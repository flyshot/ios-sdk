# Change Log

### v6.0.1 - 2021-07-05 
#### Fixed
* Redeem campaign on Producation server

### v6.0.0 - 2021-06-04 
#### Added
* QR code image recognizer
#### Added
* OpenCV pHash recognizer

### v5.0.0 - 2021-03-18 
#### Added
* Added photo's UUID to campaign
#### Updated
* Optional creatorId in campaign


### v4.2.3 - 2021-02-20 
#### Fixed 
* Return cancel error for cancel in photo picker

### v4.2.2 - 2021-02-19 
#### Fixed 
* Return cancel error for cancel in photo picker

### v4.2.1 - 2021-02-18 
#### Fixed 
* xcframework extension in podspec

### v4.2.0 - 2021-02-18
#### Added
* Error for not recognized images
#### Updated
*  Changed Cancel  state to cancel error  for upload 
#### Fixed 
* Retruns invalid for image not recognized
* Windows managment in scenes


### v4.1.0 - 2021-02-20
#### Removed
* idfa
#### Fixed
* Cancelation for Purchase Alerts
* Api  Upload canceled state
* Api  Upload image not croped
#### Updated
* Api Upload added Canceled state
* Api added initialize(sdkToken) without completion


### v4.0.1 - 2020-11-20
#### Fixed
* Api redeemed campaign 

### v4.0.0 - 2020-11-20
#### Added
* Api upload 
#### Updated
* Api getActiveCampaign renamed to getContent
* Api renamed sendConversionEvent 
#### Removed
* Api scan
* Background scan feature

### v3.0.1 - 2020-03-25
#### Fix
* API versioning

### v3.0.0 - 2020-03-25
---
#### Added
* Api: initialize 
* isExistingUser configuration
* Api for custom Promo Banner:
    * getActiveCampaign
    * scan

#### Changed
* Api: start   
* Promo scan logic

### v2.0.0 - 2020-03-02
---
#### Added
* XCFramwework for manual integration
* Appsflyer integration
* Currency for conversion events
* Redirect for inApp purchase
* API for testing SDK:
    * campaignRedeemTest
    * clearCampaignData
    * clearUserData

#### Changed
* Flyshot.framework changed to FlyshotSDK.framework
* README.md only for integration
* Usage documentation moved to https://flyshot.io/sdk/in-app-purchase
* Flyshot promo screen design
 
#### Removed
* Dependendency Alamofire
* Dependendency AlamofireImage
* Dependendency OpenCV 
