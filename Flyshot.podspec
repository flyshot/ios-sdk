#
#  Be sure to run `pod spec lint Flyshot.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "Flyshot"
  spec.version      = "6.0.1"
  spec.summary      = "Official Flyshot SDK to access Flyshot Platform's features."
  spec.homepage     = "https://bitbucket.org/flyshot/ios-sdk"

  spec.license      = { :type => "Flyshot Platform License", :file => "LICENSE" }
  spec.author       = "Flyshot"

  spec.platform     = :ios, "10.0"
  spec.source       = { :git => "https://bitbucket.org/flyshot/ios-sdk.git", :tag => "6.0.1" }

  spec.pod_target_xcconfig = { "DEFINES_MODULE" => "YES" }
  spec.requires_arc = true
  spec.swift_version = "5.0"
  spec.vendored_frameworks = "FlyshotSDK.xcframework"

end
